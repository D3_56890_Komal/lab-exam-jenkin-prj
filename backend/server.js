const express = require('express')


const app = express()

app.use(express.json())

const cors = require("cors");
app.use(cors("*"));


const routerMovie = require('./routes/movie')

// /student
app.use('/movie',routerMovie)


//Start Express Application on required port lets say 4000
app.listen(4000, () =>{
    console.log('Server Started on port 4000')
})