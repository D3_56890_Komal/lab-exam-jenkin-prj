const { query } = require('express')

const express = require ('express')

const router = express.Router()

const utils = require('../utils')

const db = require('../db')

//add a movie
router.post('/add', (request, response) => {
    const { movieTitle, movieReleaseDate, movieTime, directorName } = request.body
  
    const statement = `
          insert into Movie
            (movieTitle, movieReleaseDate, movieTime, directorName)
          values
            ( '${movieTitle}','${movieReleaseDate}','${movieTime}','${directorName}')
        `
    db.execute(statement, (error, result) => {
      response.send(utils.createResult(error, result));
    })
  })


//Display all the data of movie
router.get('/:movieId', (request, response) => {
    const{ movieId } = request.params

    const statement = `SELECT * FROM Movie where id = '${movieId}'`

    db.execute(statement, (error, result) => {
            response.send(utils.createResult(error, result))
    })
})

//Delete Movie from Containerized MySQL
router.delete('/delete/:movieId', (request, response) => {
    const{id} = request.params
    const statement = `DELETE FROM Movie WHERE movieId = ${ movieId }`

    db.execute(statement, (error, result) => {
        response.send(utils.createResult(error, result))
    })
})

//Update Update Release_Date and Movie_Time into Containerized MySQL table 
router.put('/update/:movieId', (request, response) => {
    const {movieId} = request.params
    const { movieReleaseDate, movieTime } = request.body

    const statement = `UPDATE Movie SET movieReleaseDate = '${ movieReleaseDate }', movieTime = '${movieTime}' where movieId = ${movieId}`

    db.execute(statement, (error, result) => {
        response.send(utils.createResult(error, result))
    })
})


module.exports = router;
